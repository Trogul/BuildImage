#!/bin/bash

cause_error() {
  echo >&2 "ERROR $*"
  exit 1
}

set -e

for cmd in jq aws md5sum sort find zip npm pip
do
  command -v "$cmd" >/dev/null 2>&1 || cause_error "$cmd isn't installed"
done

[[ -z "$PACKAGES_ROOT" ]] && cause_error "PACKAGES_ROOT must be set"
[[ -z "$S3_BUCKET" ]] && cause_error "S3_BUCKET must be set"
[[ -z "$REMOTE_PATH" ]] && cause_error "REMOTE_PATH must be set"

cd "$PACKAGES_ROOT" || cause_error "Unable to change directory to current directory"

while read -r module_tmp
do
  module="${module_tmp#\./}"
  echo "Working on $module"
  cd "$module" || cause_error "Unable to enter $module"
  LOCAL_FOLDER_SUM=$(find "." -type f -exec md5sum '{}' \; | sort -k2 | md5sum | awk '{print $1}')
  REMOTE_FOLDER_SUM=$(aws s3api head-object --bucket "$S3_BUCKET" --key "${REMOTE_PATH}/${module}.zip" 2>/dev/null | jq '.Metadata.folderchksum' -r)

  if [ "$LOCAL_FOLDER_SUM" != "$REMOTE_FOLDER_SUM" ]; then
    echo "Module $module needs to be uploaded again"

    if [ -e "package.json" ]; then
      echo "We need to npm install"
      npm install
    fi

    if [ -e "requirements.txt" ]; then
      echo "We need to pip install"
      pip install -r requirements.txt -t .
    fi

    zip -r "../${module}.zip" . || cause_error "Unable to zip up ${module}"
    aws s3api put-object --body "../${module}.zip" --bucket "$S3_BUCKET" --key "${REMOTE_PATH}/${module}.zip" --server-side-encryption AES256 --metadata folderchksum="${LOCAL_FOLDER_SUM}" >/dev/null || cause_error "There was an error uploading file to s3"

  else
    echo "$module does not need to be uploaded this time"
  fi

  cd ..
done < <(find "." -mindepth 1 -maxdepth 1 -type d -print)

