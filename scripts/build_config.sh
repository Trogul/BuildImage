#!/bin/sh

ENV_PREFIX="${1}_"
EXIT=0

build_config() {
    newFile=${1%.default.*}.${1#*.default.}
    echo "Moving ${1} to ${newFile}"
    cp -rpf "${i}" "${newFile}" || EXIT=1

    for i in $(printenv | grep -o -E "^${ENV_PREFIX}[a-zA-Z0-9_]+=")
    do
        var=${i%=}
        replace_var "${newFile}" "${var}"
    done

    # check for missed vars
    mkfifo missed_var_pipe
    grep -o -E '%%[a-zA-Z0-9_]+%%' "${newFile}" > missed_var_pipe &
    while read -r i
    do
        >&2 echo "    Missed ${i}"
        EXIT=1
    done < missed_var_pipe
    rm missed_var_pipe
}

replace_var() {
    key=${2#${ENV_PREFIX}}
    eval "value=\${$2}"
    echo "    Replacing ${key}"
    sed -i 's/%%'"${key}"'%%/'"${value}"'/g' "${1}" || EXIT=1
}

mkfifo file_pipe
find . -name '*.default.php' -o -name '*.default.json' > file_pipe &
while read -r i
do
    build_config "${i#./}"
done < file_pipe
rm file_pipe

exit ${EXIT}
